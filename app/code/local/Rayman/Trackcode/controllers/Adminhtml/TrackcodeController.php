<?php
 
class Rayman_Trackcode_Adminhtml_TrackcodeController extends Mage_Adminhtml_Controller_Action
{
    public function indexAction()
    {
		
        $this->loadLayout()
            ->_setActiveMenu('mycustomtab')
            ->_title($this->__('Index Action'));
 
        
		// my stuff
		//echo 'indexAction';
		$collection = Mage::getModel('trackcode/trackcode')->getCollection();
		
		
		foreach ($collection as $code) {
			//echo $code->getOrder_id();
		}
		$this->_addContent($this->getLayout()->createBlock('trackcode/adminhtml_trackcode')->setTemplate('trackcode/checkorder.phtml'));
		$this->_addContent($this->getLayout()->createBlock('trackcode/adminhtml_trackcode'));
		
        $this->renderLayout();
    }
	
	public function gridAction()
    {
        $this->loadLayout();
        $this->getResponse()->setBody(
            $this->getLayout()->createBlock('trackcode/adminhtml_trackcode_grid')->toHtml()
        );
    }	
 
	
    public function listAction()
    {
        $this->loadLayout()
            ->_setActiveMenu('mycustomtab')
            ->_title($this->__('List Action'));
 
        // my stuff
 		//echo 'listAction';
        $this->renderLayout();
    }
	
	public function ajaxAction() {
		//echo 'test';

	}
	
	public function massCheckAction() 
	{
		//rayman_trackcode id
		$ids = $this->getRequest()->getParam('id');

		$trackcode = Mage::getModel('trackcode/trackcode')->getCollection()
                ->addFieldToFilter('id', array('in' => $ids));
		
		$quote_ids = array();
		$resource = Mage::getSingleton('core/resource');
		$readConnection = $resource->getConnection('core_read');
		
		$resource = Mage::getSingleton('core/resource');
		$writeConnection = $resource->getConnection('core_write');
		
		foreach ($trackcode as $code) {
			
			$quote_id = $code->getQuote_item_id();
			$id = $code->getId();
			
			//echo $quote_id;
			//die();
			//select sfoi.order_id from sales_flat_order_item sfoi join sales_flat_quote_item sfqi on sfqi.item_id = sfoi.quote_item_id where sfqi.quote_id in (618);
			//select sfoi.order_id from sales_flat_order_item sfoi join sales_flat_quote_item sfqi on sfqi.item_id = sfoi.quote_item_id where sfqi.item_id in (2586)
			$order_id = $readConnection->fetchCol('select sfoi.order_id from sales_flat_order_item sfoi join sales_flat_quote_item sfqi on sfqi.item_id = sfoi.quote_item_id where sfqi.item_id = '.$quote_id);
			if ($order_id = implode(" ",$order_id)) {
				$writeConnection->query('update rayman_trackcode set order_id = '.$order_id.' where id = '.$id);
			} else {
				$writeConnection->query('update rayman_trackcode set order_id = \'not yet formed\' where id = '.$id);
			}
			
		}
		
		
		//var_dump($order_ids);
		//die();
		
		$this->_redirect('*/*/index');
	}
}