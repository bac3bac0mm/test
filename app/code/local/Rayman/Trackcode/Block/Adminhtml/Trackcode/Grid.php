<?php
class Rayman_Trackcode_Block_Adminhtml_Trackcode_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
		
		//echo 'grid';
        $this->setId('rayman_trackcode_grid');
        $this->setDefaultSort('increment_id');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
        $this->setUseAjax(true);
    }
	
    protected function _prepareCollection()
    {
        //$collection = Mage::getResourceModel('sales/order_collection')
		$collection = Mage::getModel('trackcode/trackcode')->getCollection();
        $this->setCollection($collection);
		$this->setDefaultSort('id');
		$this->setDefaultDir('desc');
        return parent::_prepareCollection();

    }
	
    protected function _prepareColumns()
    {
		$helper = Mage::helper('rayman_trackcode');
		
		/*$this->addColumn('id', array(
			'header_css_class' => 'a-center',
			'header' => $helper->__('1'),
			'index' => 'id',
			'name' => 'value',			
			'width'     => '5%',
			'type' => 'checkbox',
			'align' => 'center',
			'values' => array('1', '2')
		));	*/
		
		/*$this->addColumn('id', array(
            'header'    => $helper->__('id'),
            'align'     =>'left',
            'width'     => '5%',
            'index'     => 'id',
        )); */
		
		$this->addColumn('sku', array(
            'header'    => $helper->__('sku'),
            'align'     =>'left',
            'width'     => '30%',
            'index'     => 'sku',
        )); 
 		
		$this->addColumn('quote_item_id', array(
            'header'    => $helper->__('quote_item_id@sales_flat_quota_item'),
            'align'     =>'left',
            'width'     => '10%',
            'index'     => 'quote_item_id',
        ));
		
		$this->addColumn('coupon_code', array(
            'header'    => $helper->__('coupon_code'),
            'align'     =>'left',
            'width'     => '20%',
            'index'     => 'coupon_code',
        ));
		
		$this->addColumn('customer_email', array(
            'header'    => $helper->__('customer_email'),
            'align'     =>'left',
            'width'     => '20%',
            'index'     => 'customer_email',
        ));

		$this->addColumn('order_id', array(
            'header'    => $helper->__('Order_id'),
            'align'     =>'left',
            'width'     => '10%',
            'index'     => 'order_id',
        ));			
		
		$this->addColumn('created_at', array(
            'header'    => $helper->__('created_at'),
            'align'     =>'left',
            'width'     => '10%',
            'index'     => 'created_at',
        ));			
		
		/*$this->addColumn('Actions', array(
            'header'    => $helper->__('Actions'),
            'align'     =>'left',
            'width'     => '20%',
            'index'     => 'quote_item_id',
			'type'      => 'action',
            'getter'    => 'getId',			
			'actions'   => array(
                array(
                    'caption'   => 'check order',
                    'url'       => 'javascript:void(0)',
                    'field'     => 'id',
                    'href' => "javascript:void(0)",
					'target'=>'_blank',
					'class' => 'checkOrder',
					'id' =>  'id'
                )
            ),
        ));*/
				
		return parent::_prepareColumns();		
    }
	
	protected function _prepareMassaction()
	{
		$helper = Mage::helper('rayman_trackcode');
		
		$this->setMassactionIdField('id');
		$this->getMassactionBlock()->setFormFieldName('id');
	 
		$this->getMassactionBlock()->addItem('check', array(
			'label'=> $helper->__('Check order'),
			'url'  => $this->getUrl('*/*/massCheck', array('' => '')),        // public function massDeleteAction() in Mage_Adminhtml_Tax_RateController
			//'confirm' => $helper->__('Are you sure?')

		));
	 
		return $this;
		
	}	
	
	public function getGridUrl()
    {
        return $this->getUrl('*/*/grid', array('_current'=>true));
    }
	

	
	

}