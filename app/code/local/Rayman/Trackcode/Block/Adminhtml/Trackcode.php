<?php
 
class Rayman_Trackcode_Block_Adminhtml_Trackcode extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
		//echo 'block';
        $this->_controller = 'adminhtml_trackcode';
        $this->_blockGroup = 'trackcode';
        $this->_headerText = $this->__('Track code');
        
        parent::__construct();
		
		$this->_removeButton('add');
    }
	

}